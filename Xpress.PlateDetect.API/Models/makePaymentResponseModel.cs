﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xpress.PlateDetect.API.Models
{
    public class datas
    {
        public string reference { get; set; }
    }

    public class makePaymentResponseModel
    {
        public string success { get; set; }
        public string status { get; set; }
        public string message { get; set; }
        public datas data { get; set; }
    }
}