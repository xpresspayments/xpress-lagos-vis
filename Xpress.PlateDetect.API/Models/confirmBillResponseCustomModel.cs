﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xpress.PlateDetect.API.Models
{
    /* public class confirmBillResponseCustomModel
     {
     }*/

    public class datan
    {
        public string billNumber { get; set; }
        public string offender { get; set; }
        public string offence { get; set; }
        public string numberPlate { get; set; }
        public string amount { get; set; }

    }
    public class confirmBillResponseCustomModel
    {
        // public string success { get; set; }
        public string status { get; set; }
        public string message { get; set; }
        public string cap { get; set; }
        public string mscCharge { get; set; }
        public datan data { get; set; }

    }

}