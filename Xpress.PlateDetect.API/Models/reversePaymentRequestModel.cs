﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xpress.PlateDetect.API.Models
{
    public class reversePaymentRequestModel
    {
        public string reference { get; set; }
        public string reason { get; set; }
    }

    public class reversePaymentRequestModelCustom
    {
        public string reference { get; set; }
        public string reason { get; set; }
        public string billNumber { get; set; }
    }

}