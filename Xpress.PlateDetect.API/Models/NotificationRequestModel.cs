﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xpress.PlateDetect.API.Models
{
    public class NotificationRequestModel
    {
    }

    public class NotificationRequestModel1
    { 
    public string MerchantId { get; set; }
        public string MerchantName { get; set; }
        public string UniqueReferenceNo { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string Amount { get; set; }
        public string RevenueCode { get; set; }
        public string Narration { get; set; }
        public string Card { get; set; }
        public string MaskedPan { get; set; }
        public string PaymentType { get; set; }
        public string TransactionId { get; set; }
        public string ReverseId { get; set; }
        public string TransactionDate { get; set; }
        public string PaymentRefNo { get; set; }
        public string status { get; set; }
        public string Message { get; set; }
        public string NotificationStatus { get; set; }
        public string HashString { get; set; }
        public string TerminalId { get; set; }
        public string TotalAmount { get; set; }
    }

}