﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xpress.PlateDetect.API.Models
{
    public class customer
    { 
        public string emailAddress { get; set; }
        public string phoneNumber { get; set; }

    }
    public class makePaymentRequestModel
    {
        public string billNumber { get; set; }
        public string channel { get; set; }
        public string amount { get; set; }
        public string paymentDate { get; set; }
        public customer customer { get; set; }
    }
}