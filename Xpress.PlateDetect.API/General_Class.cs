﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using NLog;

namespace Xpress.PlateDetect.API
{
    public class General_Class
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public void insertToDB(string revenueCode, string mscCharge, string cap, string amount)
        {
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["cnstring"].ConnectionString);
            SqlCommand cmd = null;
            try
            {
                cn.Open();
                string totalAmount = (((Convert.ToDecimal(mscCharge) / 100) * Convert.ToDecimal(amount)) + (Convert.ToDecimal(amount))).ToString();
                string totalAmount1 = (Math.Round(Convert.ToDecimal(totalAmount), 2)).ToString();
               // String mth = string.Empty;
                string totalAmount2 = string.Empty;
                string det = ((Convert.ToDecimal(mscCharge) / 100) * Convert.ToDecimal(amount)).ToString();
                if (Convert.ToDecimal(det) < Convert.ToDecimal(cap))
                {
                    string mth = (((Convert.ToDecimal(mscCharge) / 100) * Convert.ToDecimal(amount)) + (Convert.ToDecimal(amount))).ToString();
                    totalAmount2 = Math.Round(Convert.ToDecimal(mth), 2).ToString();
                }
                else
                {
                    string mth = ((Convert.ToDecimal(cap)) + (Convert.ToDecimal(amount))).ToString();
                    totalAmount2 = Math.Round(Convert.ToDecimal(mth), 2).ToString();
                }

                cmd = new SqlCommand("IF NOT EXISTS(SELECT * FROM tbl_Lagos_VIS where revenueCode='" + revenueCode + "') BEGIN INSERT INTO tbl_Lagos_VIS(revenueCode,mscCharge,cap,amount,date,totalAmount)VALUES('" + revenueCode + "','" + mscCharge + "','" + cap + "','" + amount + "',GETDATE(),'"+totalAmount2+"') END ELSE BEGIN UPDATE tbl_Lagos_VIS SET amount='" + amount + "',totalAmount='"+totalAmount2+"' where revenueCode='" + revenueCode + "' END", cn);
                cmd.ExecuteNonQuery();
                var resp = "Insert was successful!";
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }
        }

    }
}