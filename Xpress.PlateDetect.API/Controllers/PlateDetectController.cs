﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Xpress.PlateDetect;
using System.Configuration;
using Newtonsoft.Json;
using System.Text;
using System.Threading.Tasks;
using Xpress.PlateDetect.API.Models;
using System.Net.Http.Headers;
using System.IO;
using NLog;
using System.Data;
using System.Data.SqlClient;

namespace Xpress.PlateDetect.API.Controllers
{
    public class PlateDetectController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

       // [Authorize]
        [Route("api/v1/notification")]
        public async Task<IHttpActionResult> testnotification([FromBody]NotificationRequestModel1 model)
        {
            string abst = "\"accountNumber\":\"accountNumber should be of lenght 10\",\"atmLocation\":\"please enter a valid atm location\"";

            var headerKey = Request.Headers.GetValues("apiKey").First();
            var authorizationKey = ConfigurationManager.AppSettings["Internal-apiKey"]; //Request.Headers.GetValues("apiKey");//Internal-apiKey

            if (Request.Headers == null)
            {
                return BadRequest("Authentication Key is Required!");
            }
            else if (headerKey.ToString() == null)
            {
                return BadRequest("Authentication Key is Required!");
            }
            else if (authorizationKey.ToString() != headerKey.ToString())
            {
                return BadRequest("Invalid Authentication Key!");
            }
            else
            {
                try
                {
                    if (!Directory.Exists("c:\\PLATEDETECT-LOG"))
                    {
                        Directory.CreateDirectory("c:\\PLATEDETECT-LOG");
                    }
                }
                catch (Exception ex)
                {
                    var err = ex.Message;
                }

                try
                {
                    var incomingReq = JsonConvert.SerializeObject(model);
                    logger.Warn(incomingReq + "\n\n");

                    var baseUrl1 = ConfigurationManager.AppSettings["N-baseUrl"];
                    var apiKey1 = ConfigurationManager.AppSettings["N-apiKey"];
                    var userName1 = ConfigurationManager.AppSettings["N-userName"];
                    var password1 = ConfigurationManager.AppSettings["N-password"];
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(baseUrl1);
                        client.DefaultRequestHeaders.Add("X-API-KEY", apiKey1);
                        client.DefaultRequestHeaders.Add("UserName", userName1);
                        client.DefaultRequestHeaders.Add("Password", password1);
                        client.DefaultRequestHeaders.Accept.Add(
                           new MediaTypeWithQualityHeaderValue("application/json"));



                        //StringContent content = new StringContent(JsonConvert.SerializeObject(req), Encoding.UTF8, "application/json"); //new StringContent(JsonConvert.SerializeObject(req), Encoding.UTF8, "application/json"));
                        var response = client.PostAsync("notification", new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")).Result;
                        var responseContent = await response.Content.ReadAsStringAsync();
                        //dynamic json = JsonConvert.DeserializeObject<reversePaymentResponseModel>(responseContent); //JsonConvert.DeserializeObject(responseContent);

                        logger.Warn(responseContent + "\n\n\n");

                        return Ok(responseContent);
                    }
                }
                catch (Exception ex)
                {
                    logger.Trace("\n" + ex.Message + "\n");
                    return Ok(ex.Message);
                }
            }
           // return Ok();
        }




        [Route("api/v1/confirmBill")]
        [HttpGet]
        public async Task<IHttpActionResult> confirmBill(string billNumber)
        {
            try
            {
                if(!Directory.Exists("c:\\PLATEDETECT-LOG"))
                {
                    Directory.CreateDirectory("c:\\PLATEDETECT-LOG");
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }

            var baseUrl = ConfigurationManager.AppSettings["baseUrl"];
            var apiKey = ConfigurationManager.AppSettings["apiKey"];
            var percent = ConfigurationManager.AppSettings["percent"];
            var cap = ConfigurationManager.AppSettings["cap"];
            try
            {
                var client = new HttpClient();

                client.BaseAddress = new Uri(baseUrl);
                client.DefaultRequestHeaders.Add("x-api-key", apiKey);
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                //var url = "/confirm?billNumber=1002100020335343631"; "43dwkbd843dbw843"
                HttpResponseMessage response = await client.GetAsync("confirm?billNumber=" + billNumber);
                
                var resp = await response.Content.ReadAsStringAsync();

               // string attach = "\",mscCharge\":\"0.55\",\"cap\":\"1100\""; //",\"mscCharge\":\"0.55\",\"cap\":\"1100\"";
                var DeseResponse = JsonConvert.DeserializeObject<confirmBillResponseModel>(resp);

                var dat = new datan
                {
                    billNumber=DeseResponse.data.billNumber,
                    offender=DeseResponse.data.offender,
                    offence=DeseResponse.data.offence,
                    numberPlate=DeseResponse.data.numberPlate,
                    amount=DeseResponse.data.amount
                };


                confirmBillResponseCustomModel DeseResponse1 = new confirmBillResponseCustomModel
                {
                    status = DeseResponse.status,
                    message=DeseResponse.message,
                    mscCharge="0.55",
                    cap="1100"                    
                };
                DeseResponse1.data = dat;

                var deseResponseError = new
                {
                    status = "Error",
                    message = "Bill not found!"
                };

                logger.Error(resp+"\n\n\n");
                if (DeseResponse.status=="success")
                {
                    General_Class gclass = new General_Class();
                    // gclass.insertToDB(DeseResponse.data.billNumber, "0.55", "1100", DeseResponse.data.amount);
                    gclass.insertToDB(DeseResponse.data.billNumber, percent, cap, DeseResponse.data.amount);
                    return Ok(DeseResponse1);
                }
                else
                {
                    
                    return Ok(deseResponseError);
                }
            }
            catch (Exception ex)
            {
                logger.Trace("\n" + ex.Message + "\n");
                var deseResponseError = new
                {
                    status = "Error",
                    message = "Bill not found!"
                };
                return Ok(deseResponseError);
                //  return Ok(ex.Message);
            }
            
        }

        [Route("api/v1/Payment")]
        [HttpPost]
        public async Task< IHttpActionResult> makePayment([FromBody]makePaymentRequestModel model)
        {

            try
            {
                if (!Directory.Exists("c:\\PLATEDETECT-LOG"))
                {
                    Directory.CreateDirectory("c:\\PLATEDETECT-LOG");
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }

            try
            {
                var incomingReq = JsonConvert.SerializeObject(model);
                logger.Info(incomingReq + "\n\n");

                var baseUrl = ConfigurationManager.AppSettings["baseUrl"];
                var apiKey = ConfigurationManager.AppSettings["apiKey"];
                var userName = ConfigurationManager.AppSettings["userName"];
                var password = ConfigurationManager.AppSettings["password"];
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Add("X-API-KEY", apiKey);
                    client.DefaultRequestHeaders.Add("UserName", userName);
                    client.DefaultRequestHeaders.Add("Password", password);
                    client.DefaultRequestHeaders.Accept.Add(
                       new MediaTypeWithQualityHeaderValue("application/json"));
                    //StringContent content = new StringContent(JsonConvert.SerializeObject(req), Encoding.UTF8, "application/json"); //new StringContent(JsonConvert.SerializeObject(req), Encoding.UTF8, "application/json"));
                    var response = client.PostAsync("notify?billNumber=" + model.billNumber, new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")).Result;
                    var responseContent = await response.Content.ReadAsStringAsync();
                    dynamic json = JsonConvert.DeserializeObject<makePaymentResponseModel>(responseContent); //JsonConvert.DeserializeObject(responseContent);

                    logger.Info(responseContent + "\n\n\n");

                    return Ok(json);
                }
            }
            catch (Exception ex)
            {
                logger.Trace("\n" + ex.Message + "\n");
                return Ok(ex.Message);
            }

        }

        [Route("api/v1/CancelPayment")]
        [HttpPost]
        public async Task<IHttpActionResult> revertPayment([FromBody]reversePaymentRequestModelCustom model)
        {

            try
            {
                if (!Directory.Exists("c:\\PLATEDETECT-LOG"))
                {
                    Directory.CreateDirectory("c:\\PLATEDETECT-LOG");
                }
            }
            catch (Exception ex)
            {
                var err = ex.Message;
            }

            try
            {
                var incomingReq = JsonConvert.SerializeObject(model);
                logger.Info(incomingReq + "\n\n");

                var baseUrl = ConfigurationManager.AppSettings["baseUrl"];
                var apiKey = ConfigurationManager.AppSettings["apiKey"];
                var userName = ConfigurationManager.AppSettings["userName"];
                var password = ConfigurationManager.AppSettings["password"];
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Add("X-API-KEY", apiKey);
                    client.DefaultRequestHeaders.Add("UserName", userName);
                    client.DefaultRequestHeaders.Add("Password", password);
                    client.DefaultRequestHeaders.Accept.Add(
                       new MediaTypeWithQualityHeaderValue("application/json"));

                    var req = new reversePaymentRequestModel
                    {
                        reference = model.reference,
                        reason = model.reason
                    };

                    //StringContent content = new StringContent(JsonConvert.SerializeObject(req), Encoding.UTF8, "application/json"); //new StringContent(JsonConvert.SerializeObject(req), Encoding.UTF8, "application/json"));
                    var response = client.PostAsync("reverse?billNumber=" + model.billNumber, new StringContent(JsonConvert.SerializeObject(req), Encoding.UTF8, "application/json")).Result;
                    var responseContent = await response.Content.ReadAsStringAsync();
                    dynamic json = JsonConvert.DeserializeObject<reversePaymentResponseModel>(responseContent); //JsonConvert.DeserializeObject(responseContent);

                    logger.Info(responseContent + "\n\n\n");

                    return Ok(json);
                }
            }
            catch (Exception ex)
            {
                logger.Trace("\n" + ex.Message + "\n");
                return Ok(ex.Message);
            }
        }

        // GET: api/PlateDetect
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/PlateDetect/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/PlateDetect
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/PlateDetect/5
        public void Put(int id, [FromBody]string value)
        {
        }

        [Route("api/v1/fake")]
        [HttpPost]       
        public async Task asss(drsResponse model)
        {

            drsResponse dresp = new drsResponse();
          //  for (int i = 0; i < dresp.data.Count; i++)   
          foreach(var item in model.data)
            {
                string query = "UPDATE tbl_disputeResol set responseCode='01',responseMessage='" + item.error.accountName + item.error.accountNumber + item.error.acquirerBank + item.error.acquirerRRN + item.error.amount + item.error.approvalAuthorizationCode + item.error.ARN + item.error.atmLocation + item.error.dateOfSettlement + item.error.dateOfTransaction + item.error.issuerRRN + item.error.issuingBank + item.error.maskedPAN + item.error.merchantID + item.error.merchantName + item.error.processorID + item.error.processorIDTransactionID + item.error.STAN + item.error.terminalID + item.error.transactionChannelID + item.error.otherDetails + "' WHERE accountNumber='" + item.dataObject.accountNumber + "' and amount='" + item.dataObject.amount + "' and dateOfTransaction='" + item.dataObject.dateOfTransaction + "' ";
            }

            var destr = "gj";

            var request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://platedetecttest.softalliance.com/api/v1/payments/confirm?billNumber=1002100020335343631");
            request.Timeout = 100000;
            request.Method = "GET";
            request.Headers.Add("Authorization", "x-api-key 43dwkbd843dbw843");
            request.ContentType = "application/json";
            request.Accept = "application/json";

            

            try
            {
                 var client = new HttpClient();

                client.BaseAddress = new Uri("http://platedetecttest.softalliance.com/api/v1/payments/");
                client.DefaultRequestHeaders.Add("x-api-key", "43dwkbd843dbw843");
                client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));

                var url = "confirm?billNumber=1002100020335343631";
                HttpResponseMessage response = await client.GetAsync(url);
                response.EnsureSuccessStatusCode();
                var resp = await response.Content.ReadAsStringAsync();
            }
            catch (System.Net.WebException e)
            {
                var error = e.Message;
               // Print(e.Message);
            }
        }

        // DELETE: api/PlateDetect/5
        public void Delete(int id)
        {
        }
    }
}
